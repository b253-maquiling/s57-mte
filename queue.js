let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(x) {
  collection.push(x);
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  x = collection.length;
  return x;
}

function isEmpty() {
  return !collection;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
